# Event Details

* **FMM Owner:** FMM Name @fmmusername
* **Type:** Owned Event
* **Official Event Name:** GitLab Connect [city]
* **Date:** 
* **Location:** 
* **Website:** 
* **Budgeted Cost:** `FMM fill in`

#### What will this event contain/require? - if "MAYBE" please indicate estimated date of decision.
⚠️ **This section must be filled out by the FMM before the MPM creates the epic and related issues. [See handbook.](https://about.gitlab.com/handbook/marketing/marketing-sales-development/marketing-programs/#events-process-for-marketing-programs--marketing-ops)**
* Follow up email via Marketo - YES/NO
* Will the copy rely on content from the event? i.e. copy due 24 hours post-event (this will push out the send 2 business days). - YES/NO
* Add event leads to nurture stream? - YES/NO (please specify nurture stream) [see nurture options here](https://about.gitlab.com/handbook/marketing/marketing-sales-development/marketing-programs/#top-funnel-nurture)
* Landing page creation (requires promotion plan) - YES/NO
* Invitation and reminder emails (requires promotion plan) - YES/NO
* Alliances/Partner Marketing involved - YES/NO/MAYBE

## GitLab Connect

#### Event Details: 
What is GitLab Connect - Full day or half day event with both customers and prospects in attendance sharing stories & lessons learned about GitLab. SAL's will be responsible for asking customers to speak and Marketing, through a combination of XDR outreach, database and ad geotargeting will drive attendance to the event. 

* External Registration page: 
* Salesforce campaign: 
* Location: 
* Date:
* Proposed half day agenda: 
	- 12:00 PM - Check in begins
	- 12:00 PM - Lunch begins
	- 12:35 PM - Welcome from GitLab SAL 
	- 12:40 PM - Customer Story
	- 1:00 PM - GitLab topic from SA - Roadmap & topics from audience 
	- 1:40 PM - User Story
	- 2:00 PM - Break 
	- 2:30 PM - User Story 
	- 3:00 PM - User Story 
	- 3:30 PM - Happy hour with food & drinks/Close  
	
* Proposed full day agenda: 
	- 9:00AM - check-in/coffee/nosh/network
	- 9:30AM  - Keynote/intro - GitLab
	- 10:00AM - User Story
	- 10:30AM - Break
	- 10:45AM - User Story 
	- 11:15AM - User Story 
	- 11:50AM - Lunch
	- 12:35PM - User Story
	- 1:05PM - GitLab topic
	- 1:35PM - Break
	- 1:45PM - User Story
	- 3:15PM - Closing
	- 4:00PM - Happy hour with food & drinks 


#### Speakers: 
Ideally there would be at least 3 speakers for half day event and 5 for full day event. 

- [ ] Customer Name & use case/area he/she will be speaking about
- [ ] Customer Name & use case/area he/she will be speaking about
- [ ] Customer Name & use case/area he/she will be speaking about
- [ ] Customer Name & use case/area he/she will be speaking about
- [ ] Customer Name & use case/area he/she will be speaking about

#### Speaker outreach verbiage 
Hi NAME of CUSTOMER: We are hosting a GitLab Day in [LOCATION] on [give date or propose a few dates] and I think your use case on [XYZ] is very compelling for your peers to hear. Are you willing to share your story in a 30 minute speaking slot? Additionally, I am hoping that by attending a GitLab Day you and your peers will build and expand your network of GitLab users, so you'll be able to share best practices. 

I am happy to talk through GitLab day and your use case if you'd like. How does [XYZ] date and [XYZ] time work? 

Sincerely, 
[your name]

#### Target Attendees 

##### Existing Customers 
- [ ]
- [ ]

##### Prospects
- [ ]
- [ ]

### Attendees:**Staffing Needs:**
   * [ ] SAL:
   * [ ] SDR:
   * [ ] PMM:
   * [ ] other:

# Pre Event:
   * [ ] added to relevant boards
   * [ ] added to Events calendar
   * [ ] event added to attendee calendars
   * [ ] MPM issue created
   * [ ] social media issue created (if necessary)
   * [ ] slack channel
   * [ ] planning doc created and shared w all parties
   * [ ] event planning meetings scheduled (kick off, final prep meeting & post mortem)
   * [ ] design requested
   * [ ] swag ordered

# Post Event
* [ ] Event recap & Feedback [Template](https://docs.google.com/spreadsheets/d/1i2-CdlsvW2x98NvJJ1mLcVq6ymehaRqSu2ckkWiV5ko/edit?usp=sharing)

# Financial: 
* [ ] legal issue: 
* [ ] MPM to add expense tracking code for [Expensify](https://about.gitlab.com/handbook/marketing/marketing-sales-development/marketing-operations/#campaign-cost-tracking) - tag matches naming convention used for SFDC, email to `ap@` - Subject: Campaign Finance Tag Creation Request
* [ ] Contract signed (add to appropriate contract folder)
* [ ] Invoice paid

## TO DO

* `Assign the issue to the FMM in addition to Jackie and Agnes
* `Assign the Quarter and Region tags to the issue`

cc @jjcordz

/assign @jgragnola @aoetama

/label ~Events ~"Field Marketing"  ~"MktgOPS \- FYI" ~"MPM - Radar" ~"Marketing Programs" ~"Marketing Campaign"