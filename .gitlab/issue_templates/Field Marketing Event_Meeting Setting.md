# Event Details


**FMM Owner:** FMM Name @fmmusername

**Type:** Field Event

**Name of Conference** (if being run in conjunction with a corporate event, please mark this issue as related to the corporate issue)

**SFDC Campaign:**

**PLANNING DOC** (link here)

**Dates/Times Available:**

**Meeting Room Location:** 

**Target audience:**

#### Deadline for Scheduling: All meetings need to be scheduled and approved by (enter date here).  After that the doc will be locked and any changes and/or additions will need to be made onsite by (enter onsite manager).

#### General Directions (please read before proceeding)
The meeting planning doc is comment only, so when entering your meeting info, you will need to tag the FMM (for non c-suite) for the meeting to be approved.  This avoids confusion and overrides of existing meetings.  For C-suite meetings, your first reach out should be directly to the EA of the person you are trying to schedule with and they will coordinate the booking of the meeting.
** [Example of Client Meeting Prep doc that Sid liked](https://docs.google.com/document/d/1KNWMkwjIta8sAGtaRH5JeE6sV7vLyf2NyYRIEyzMnkU/edit)**

#### Directions for booking (if no C-suite):

1. Once you have a meeting time, Enter your meeting with full customer information into planning doc linked above in this issue (including GitLab attendees) in a comment in the time you would like.  If the meeting is pending, please put pending in parenthesis beforehand so we know it is a placeholder (once confirmed, remove this).
2. AE completes and attaches a completed [Client Meeting Prep](https://docs.google.com/document/d/1QZB8m99Lt5GVnBDcfST1otmA81uH6EMEMkF-1zhQ0q0/edit) doc to the timeslot.
3. Assign your timeslot in the doc for approval to the FMM in charge. The FMM will approve ASAP, but no later than 24hours after the request is made.
4. Once the meeting is approved, send calendar invite to all included (both customer and Gitlab attendees) in the meeting and include the address.  Directions to add for attendees are below and should be copy and pasted into the invite.
5. Post meeting: please update the meeting notes doc no later than 24 hours post meeting

#### Directions for booking (if includes C-suite)

1. Internal rep must reach out to the EA supporting the Exec to coordinate the meeting - [Client meeting prep](https://docs.google.com/document/d/1QZB8m99Lt5GVnBDcfST1otmA81uH6EMEMkF-1zhQ0q0/edit) doc is due at the time of the request so we may evaluate the meeting and determine the best audience (whether that's McB, Todd, Sid, etc..). External contact should be a Director level and above to meet with members of our C-Suite (CEO, CRO, CMO, CFO, CCO). 
2. If EA determines we are proceeding with the meeting, Sales rep introduces EA via e-mail to their external contacts and EA blocks the time in the planning spreadsheet as a *HOLD*. 
3. EA will coordinate the meeting with external party and send invite from Exec's calendar, the meeting brief document will be shared in this invite with all parties. Once the meeting is confirmed and scheduled the EA will update the planning spreadsheet with "CONF" . EA and Sales Rep to be on-site to staff the meeting at larger conferences (AWS/ DOES/ KubeCon). If sales rep will not be attending the meeting in-person they need to notify the EA asap. 

#### Directions for meeting attendees (copy and paste into your confirmation email to attendees)
   Enter instructions here
   
#### FMM to complete   
   
* [ ] Directions for booking added
* [ ] location confirmed
* [ ] dates/times available confirmed
* [ ] directions for booking in issue and planning doc

## TO DO

* `Assign the issue to the FMM in addition to Jackie or Agnes (who should be autoassigned)`
* `Assign the Quarter and Region tags to the issue`

cc @jjcordz

/assign @jgragnola @aoetama

/label ~Events ~"Field Marketing"  ~"MktgOPS \- FYI" ~"MPM - Radar" ~"Marketing Programs" ~"Marketing Campaign"


