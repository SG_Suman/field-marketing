#### Request an event:
This section is to be used by anyone internally requesting GitLab sponsor an event. 
* Name of event:
* Event website: 
* Justification for wanting to sponsor the event (please be as detailed as possible): 
* If we went last year, link to SF.com campaign:  

 **Everything below this section will be updated once the event has been approved.** 

 ### Event Details
* SFDC campaign:
* Location: 
* Speakers: 
* Number of Attendees:
* Staffing Needs:
* Sponsorship:
* Lodging: 
* Expo Center: 
* Dress code:
* Audience Demographics/ Breakdown:
* Goals:
* Other Benefits of attending/ sponsorship:
* Total costs (sponsorship + travel + collateral + etc.):

### Info for Marketing Programs Manager
* **FMM Owner:** FMM Name @fmmusername
* **Type:** (select one) Conference, Owned Event, Field Event, Speaking Session - [*see handbook for event type definitions*](https://about.gitlab.com/handbook/marketing/marketing-sales-development/marketing-programs/#Requesting-Marketing-Programs-Support-for-a-Field-Event)
* **Official Event Name:** 
* **Date:** 
* **Location:** 
* **Website:** 
* **Budgeted Cost:** `FMM fill in`

#### What will this event contain/require? - if "MAYBE" please indicate estimated date of decision.
⚠️ **This section must be filled out by the FMM before the MPM creates the epic and related issues. [See handbook.](https://about.gitlab.com/handbook/marketing/marketing-sales-development/marketing-programs/#events-process-for-marketing-programs--marketing-ops)**
* Follow up email via Marketo - YES/NO
* Will the copy rely on content from the event? i.e. copy due 24 hours post-event (this will push out the send 2 business days). - YES/NO
* Add event leads to nurture stream? - YES/NO (please specify nurture stream) [see nurture options here](https://about.gitlab.com/handbook/marketing/marketing-sales-development/marketing-programs/#top-funnel-nurture)
* Landing page creation (requires promotion plan) - YES/NO
* Invitation and reminder emails (requires promotion plan) - YES/NO
* Speaking session - YES/NO/MAYBE
* Workshop - YES/NO/MAYBE
* Dinner - YES/NO/MAYBE
* Party/Happy Hour - YES/NO/MAYBE
* Alliances/Partner Marketing involved - YES/NO/MAYBE

## Invitation plan:
* [Review the Field Marketing handbook](https://about.gitlab.com/handbook/marketing/marketing-sales-development/field-marketing/#event-operations-checklist-before-event) on how to add members to a campaign & have marketing send an email for you. 

* Will marketing send email on SALs behalf: Y/N 

### Campaign Checklist - Marketing Programs  
* Event budget: 
* goals for # of leads: 
* goals for # SCLAU: 
* Number of meetings at event: 
* registration goals/ attendance goals: 

🔗 **Please see all relevant links, action items, and timelines pertaining to MPM Support in this related issue: [link tbd when created](http://gitlab.com/)**

### Speaker Checklist
* [ ] Speaker brief + bio sent
* [ ] Slides reviewed by content and design
* [ ] Speaker Slides Sent (Deadline: )
* [ ] Speaker final run through scheduled

### Outreach Checklist/ At Event Meeting Plan - Marketing Programs (w/ Mktg OPS support)
* [ ] Calendar links provided to XDR team for outreach- Create Meeting Schedule in outreach
* [ ] [Client Meeting Prep Template](https://docs.google.com/document/d/1QZB8m99Lt5GVnBDcfST1otmA81uH6EMEMkF-1zhQ0q0/edit#) must be completed for every on site meeting by AE
* [ ] Target customers/ prospects identified

Post Event:
* [ ] Leads shared with team (24 hours after event close)
* [ ] Leads uploaded to campaign and list locked (all changes after lock to be made in SFDC)
* [ ] Sales notified of lead/contact assignments - auto-assign to match named accounts 
* [ ] After event follow up launched
* [ ] After event survey sent

### Event Checklist
* [ ] Contract Signed
* [ ] Event added to Events Cal and Events page
* [ ] Invoice paid
* [ ] Artwork sent
* [ ] Booth Artwork sent 
* [ ] Slack channel created and attendees invited
* [ ] Planning spreadsheet (included travel, booth duty, meetings...)
* [ ] Tickets allocated   
* [ ] Attendee directory from organizers
* [ ] Press list sent 
* [ ] Badge scanners
* [ ] AV ordered
* [ ] Furnishings ordered
* [ ] Electrical if needed
* [ ] Booth duty scheduling
* [ ] Social media copy written and scheduled @evhoffmann 
* [ ] Flights/ transport booked- added to spreadsheet
* [ ] Booth slideshow/ demo (shared with staff)
* [ ] Final prep meeting scheduled
* [ ] Event post mortem scheduled 
* [ ] After event follow up set up in Marketo

### Swag checklist:
* [ ] swag ordered
* Shipping Address:
* In hand date and time:
* Item/ Color/ Volume:
* Price per item:
* Total $: 
* Tracking: 

* Mens
  
| XS | Sm | Med | Lg | XL | XXL | XXXL |
|----|----|-----|----|----|-----|------|
|    |    |     |    |    |     |      |
  
* Women's
   
| XS | Sm | Med | Lg | XL | XXL | XXXL |
|----|----|-----|----|----|-----|------|
|    |    |     |    |    |     |      |

## Expo Schedule:


## Other:

## Financial approval
* [ ] Legal issue number: 
* [ ] Exec approval (if cost is over $10,000):  
* [ ] Spend is documented in non-headcount budget: row XYZ

## TO DO

* `Assign the issue to the FMM in addition to Jackie and Agnes (who should be autoassigned)`
* `Assign the Quarter and Region tags to the issue`

cc @jjcordz @emily

/assign @jgragnola @aoetama

/label ~Events ~"Field Marketing"  ~"MktgOPS \- FYI" ~"MPM - Radar" ~"Marketing Programs" ~"Marketing Campaign"