 
# Event Details

**FMM Owner:** @fmmusername

**Type:** Field Event

**Official Event Name:** 

**Planning Doc:**

**Name of Conference** (if being run in conjunction with a corporate event, please mark this issue as related to the corporate issue)

**Date:** 

**Time:**

**Location:** 

**Budgeted Cost:** 

**Capacity:**

**Description:**

**SFDC Campaign:**

[**RSVP List**](pull campaign URL from SFDC )

**GitLab Attendees:**
*  [ ] SAL:
*  [ ] SDR:
*  [ ] PMM:
*  [ ] other:

#### What will this event contain/require? - if "MAYBE" please indicate estimated date of decision.
⚠️ **This section must be filled out by the FMM before the MPM creates the epic and related issues. [See handbook.](https://about.gitlab.com/handbook/marketing/marketing-sales-development/marketing-programs/#events-process-for-marketing-programs--marketing-ops)**
* Follow up email via Marketo - YES/NO
* Will the copy rely on content from the event? i.e. copy due 24 hours post-event (this will push out the send 2 business days). - YES/NO
* Add event leads to nurture stream? - YES/NO (please specify nurture stream) [see nurture options here](https://about.gitlab.com/handbook/marketing/marketing-sales-development/marketing-programs/#top-funnel-nurture)
* Landing page creation (requires promotion plan) - YES/NO
* Invitation and reminder emails (requires promotion plan) - YES/NO
* Alliances/Partner Marketing involved - YES/NO/MAYBE

# Invitation Plan for event

1. Reach out and invite (suggested language below, please modify as needed)


2. Add confirmed folks' name to the planning doc linked at the top of this issue and @fmmanager will send them a calendar invite with event details


3. @fmmanager / marketing will send a confirmation "see you there" email to all who have accepted the invite one day before the dinner


**Target Attendee Profile:**
  
* [ ] Existing Customers
* [ ] Prospects
* [ ] Other (i.e. specific accounts, speakers at conference etc. Link list here if we have one)

# Pre Event:
*  [ ] added to relevant boards
*  [ ] marked as related to conference issue
*  [ ] conference planning doc linked in issue
*  [ ] added to Events calendar
*  [ ] invite plan outline in issue and added to conference planning doc
*  [ ] calendar invite created for attendees
*  [ ] event added to GitLab attendee calendars
*  [ ] MPM issue created
*  [ ] social media issue created (if necessary)
*  [ ] event planning meetings scheduled (kick off, final prep meeting & post mortem)


# Post Event
* [ ] Event recap & Feedback [Template](https://docs.google.com/spreadsheets/d/1i2-CdlsvW2x98NvJJ1mLcVq6ymehaRqSu2ckkWiV5ko/edit?usp=sharing)

# Financial: 
* [ ] legal issue: 
* [ ] MPM to add expense tracking code for [Expensify](https://about.gitlab.com/handbook/marketing/marketing-sales-development/marketing-operations/#campaign-cost-tracking) - tag matches naming convention used for SFDC, email to `ap@` - Subject: Campaign Finance Tag Creation Request
* [ ] Contract signed (add to appropriate contract folder)
* [ ] Invoice paid

## TO DO

* `Assign the issue to the FMM in addition to Jackie or Agnes (who should be autoassigned)`
* `Assign the Quarter and Region tags to the issue`

cc @jjcordz

/assign @jgragnola @aoetama

/label ~Events ~"Field Marketing"  ~"MktgOPS \- FYI" ~"MPM - Radar" ~"Marketing Programs" ~"Marketing Campaign"
