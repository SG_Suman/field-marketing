# If you are requesting the event:
This section is to be used by anyone internally requesting GitLab sponsor an event. 
* Name of event:
* Event website: 
* Justification for wanting to sponsor the event (please be as detailed as possible): 
* If we went last year, link to SF.com campaign:  

 **Everything below this section will be updated once the event has been approved.** 
# Event Details

* **FMM Owner:** FMM Name @fmmusername
* **Type:** (select one) Conference, Owned Event, Field Event, Speaking Session - [*see handbook for event type definitions*](https://about.gitlab.com/handbook/marketing/marketing-sales-development/marketing-programs/#Requesting-Marketing-Programs-Support-for-a-Field-Event)
* **Official Event Name:** 
* **Date:** 
* **Location:** 
* **Website:** 
* **Budgeted Cost:** `FMM fill in`

#### What will this event contain/require? - if "MAYBE" please indicate estimated date of decision.
⚠️ **This section must be filled out by the FMM before the MPM creates the epic and related issues. [See handbook.](https://about.gitlab.com/handbook/marketing/marketing-sales-development/marketing-programs/#events-process-for-marketing-programs--marketing-ops)**
* Follow up email via Marketo - YES/NO
* Will the copy rely on content from the event? i.e. copy due 24 hours post-event (this will push out the send 2 business days). - YES/NO
* Add event leads to nurture stream? - YES/NO (please specify nurture stream) [see nurture options here](https://about.gitlab.com/handbook/marketing/marketing-sales-development/marketing-programs/#top-funnel-nurture)
* Landing page creation (requires promotion plan) - YES/NO
* Invitation and reminder emails (requires promotion plan) - YES/NO
* Speaking session - YES/NO/MAYBE
* Workshop - YES/NO/MAYBE
* Dinner - YES/NO/MAYBE
* Party/Happy Hour - YES/NO/MAYBE
* Alliances/Partner Marketing involved - YES/NO/MAYBE

**Description of event:**

**SFDC Campaign:**

[**RSVP List**](pull campaign URL from SFDC )

**GitLab presence (internal event or sponsorship details)**

**Staffing Needs:**
   * [ ] SAL:
   * [ ] SDR:
   * [ ] SA: 
   * [ ] other:
  
**Travel** (if travel is required please complete this section.  if not, just delete)

   * [ ] Travel dates:
   * [ ] Link to hotel block (if applicable):
   * [ ] DEADLINE to book your travel:

## Goals for event:

## Does this event have a speaking engagement?
**(Description of speaking engagement)**

**Speaker Checklist**
* [ ] Speaker brief + bio sent
* [ ] Slides reviewed by content and design
* [ ] Speaker Slides Sent (Deadline: )
* [ ] Speaker final run through scheduled

## Outreach
**(Outline the invitation plan for event)**
* [ ] Marketing to send invites
* [ ] Sales to send invite

**Target Attendees:**
* [ ] Existing Customers: 
* [ ] Prospects:
* [ ] Other: 

## Pre Event:
   * [ ] added to relevant boards
   * [ ] added to Events calendar
   * [ ] added to events page on website
   * [ ] event added to attendee calendars
   * [ ] MktgOPS issue created
   * [ ] social media issue created (if necessary)
   * [ ] slack channel 
   * [ ] planning doc created and shared w all parties
   * [ ] event planning meetings scheduled (kick off, final prep meeting)
   * [ ] design requested
   * [ ] swag ordered


## Post Event
* [ ] Event recap & Feedback [Template](https://docs.google.com/spreadsheets/d/1i2-CdlsvW2x98NvJJ1mLcVq6ymehaRqSu2ckkWiV5ko/edit?usp=sharing)

## Financial: 
* [ ] legal issue: 
* [ ] MPM to add expense tracking code for [Expensify](https://about.gitlab.com/handbook/marketing/marketing-sales-development/marketing-operations/#campaign-cost-tracking) - tag matches naming convention used for SFDC, email to `ap@` - Subject: Campaign Finance Tag Creation Request
* [ ] Contract signed (add to appropriate contract folder)
* [ ] Invoice paid

## TO DO

* `Assign the issue to the FMM in addition to Jackie (Americas) or Agnes (EMEA/APAC) (who should be autoassigned)`
* `Assign the Quarter and Region tags to the issue`

cc @jjcordz

/assign @jgragnola @aoetama

/label ~Events ~"Field Marketing"  ~"MktgOPS \- FYI" ~"MPM - Radar" ~"Marketing Programs" ~"Marketing Campaign"
